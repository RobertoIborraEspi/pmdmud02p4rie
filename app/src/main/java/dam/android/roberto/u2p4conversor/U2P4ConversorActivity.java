package dam.android.roberto.u2p4conversor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

// TODO: Ex3 Extend LogAcivity
public class U2P4ConversorActivity extends LogActivity {

    private static final String DEBUG_TAG = "LogsAndroid-1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u2_p4_conversor);
        setUI();
    }
    private void setUI(){
        EditText input = findViewById(R.id.et_pulgadas);
        EditText reault = findViewById(R.id.et_centimetros);
        Button convertirp = findViewById(R.id.button_convertir_p);
        // TODO: Ex2 >=1
        TextView error = findViewById(R.id.textErr);
        convertirp.setOnClickListener(view -> {
            // TODO: Ex3 Log message for each button
            Log.i(DEBUG_TAG, "LOG-Button Convertir a centimetros" + " " + getLocalClassName());
            try{
                reault.setText(convertirP2CM(input.getText().toString()));
                error.setText("");
                error.setVisibility(View.INVISIBLE);
            }catch (Exception e){
                Log.e("LogsConversor", e.getMessage());
                error.setText(e.getMessage());
                error.setVisibility(View.VISIBLE);
            }
        });
        // TODO: Ex1 Two-way conversion inch <-> cm
        Button convertircm = findViewById(R.id.button_convertir_cm);
        convertircm.setOnClickListener(view -> {
            Log.i(DEBUG_TAG, "LOG-Button Convertir a pulgadass" + " " + getLocalClassName());
            try{
                reault.setText(convertirCM2P(input.getText().toString()));
                error.setText("");
                error.setVisibility(View.INVISIBLE);
            }catch (Exception e){
                Log.e("LogsConversor", e.getMessage());
                error.setText(e.getMessage());
                error.setVisibility(View.VISIBLE);
            }
        });
    }
    // TODO: Ex1 Two-way conversion inch <-> cm
    private String convertirP2CM(String pulgadas) throws Exception {
        // TODO: Ex1 Round with two decimals
        // TODO: Ex2 >=1
        if (Integer.parseInt(pulgadas) >= 1) {
            return String.valueOf(Math.round((Double.parseDouble(pulgadas) * 2.54)*100.0)/100.0);
        }else throw new Exception("Sólo números >=1");
    }
    private String convertirCM2P(String centimetros) throws Exception {
        // TODO: Ex1 Round with two decimals
        // TODO: Ex2 >=1
        if (Integer.parseInt(centimetros) >= 1) {
            return String.valueOf(Math.round((Double.parseDouble(centimetros) / 2.54)*100.0)/100.0);
        }else throw new Exception("Sólo números >=1");
    }
    // TODO: Ex4 Error disapears when rotate
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        TextView error = findViewById(R.id.textErr);
        error.setText(savedInstanceState.getString("error"));
        error.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        TextView error = findViewById(R.id.textErr);
        outState.putString("error", error.getText().toString());
        super.onSaveInstanceState(outState);
    }
}