package dam.android.roberto.u2p4conversor;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class LogActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "LogsAndroid-1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(DEBUG_TAG, "LOG-onCreate" + " " + getLocalClassName());
        notify("onCreate");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG, "LOG-onStart" + " " + getLocalClassName());
        notify("onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG, "LOG-onStop" + " " + getLocalClassName());
        notify("onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG, "LOG-onPause" + " " + getLocalClassName());
        notify("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG, "LOG-onResume" + " " + getLocalClassName());
        notify("onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG, "LOG-onRestart" + " " + getLocalClassName());
        notify("onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       if (isFinishing()) {
           Log.i(DEBUG_TAG, "LOG-onDestroy" + " " + getLocalClassName() + " Finished by User");
       }else  Log.i(DEBUG_TAG, "LOG-onDestroy" + " " + getLocalClassName() + " Finished by System");
        notify("onDestroy");
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(DEBUG_TAG, "LOG-onSaveInstanceState" + " " + getLocalClassName());
        notify("onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(DEBUG_TAG, "LOG-onRestoreInstanceState" + " " + getLocalClassName());
        notify("onRestoreInstanceState");
    }

    private void notify (String eventName) {
        String activityName = this.getClass().getSimpleName();
        String CHANNEL_ID = "My_LifeCycle";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "My LifeCycle",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription("Lifecycle events");
            notificationChannel.setShowBadge(true);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName + " " + activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);
        notificationManagerCompat.notify((int) System.currentTimeMillis(), notificationBuilder.build());
    }
}